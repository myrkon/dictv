using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Text.Json;

namespace core {
	class Utils {
		public static StreamReader GetStreamReader(FileInfo dictFile) {
			StreamReader sr;
			if(dictFile.ToString().EndsWith(".dsl.dz"))
				sr = new StreamReader(new GZipStream(dictFile.OpenRead(), CompressionMode.Decompress));
			else
				sr = new StreamReader(dictFile.OpenRead());

			return sr;
		}

		public static Encoding GetEncoding(FileInfo fileInfo) {
			// Read the BOM
			var bom = new char[4];
			using (var file = GetStreamReader(fileInfo))
			{
				file.Read(bom, 0, 4);
			}

			if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return Encoding.UTF8;
			if (bom[0] == 0xff && bom[1] == 0xfe && bom[2] == 0 && bom[3] == 0) return Encoding.UTF32; //UTF-32LE
			if (bom[0] == 0xff && bom[1] == 0xfe) return Encoding.Unicode; //UTF-16LE
			if (bom[0] == 0xfe && bom[1] == 0xff) return Encoding.BigEndianUnicode; //UTF-16BE
			if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return new UTF32Encoding(true, true);	//UTF-32BE

			// We actually have no idea what the encoding is if we reach this point, so
			// you may wish to return null instead of defaulting to ASCII
			return Encoding.ASCII;
		}

		public static void WriteJson<T>(T serializableValue, string filePath) {
			File.WriteAllText(filePath, JsonSerializer.Serialize(serializableValue));
		}

		public static T? ReadJson<T>(string filePath) {
			return JsonSerializer.Deserialize<T>(File.ReadAllText(filePath));
		}

	}
}
