﻿using System.Collections.Generic;
using ReactiveUI;

namespace DictV.ViewModels
{
	public class MainWindowViewModel : ViewModelBase {
		public List<string> SideBarItems { get; set; }
		public bool IsDictSettignsViewVisible {
			get => _isDictSettingsViewVisible;
			set => this.RaiseAndSetIfChanged(ref _isDictSettingsViewVisible, value);
		}
		public bool IsTransalteViewVisible {
			get => _isTranslateViewVisible;
			set => this.RaiseAndSetIfChanged(ref _isTranslateViewVisible, value);
		}
		public ViewModelBase Content {
			get => _content;
			set => this.RaiseAndSetIfChanged(ref _content, value);
		}
		public TranslateViewModel TranslateView  {
			get => _translateViewModel;
			set => this.RaiseAndSetIfChanged(ref _translateViewModel, value);
		}
		public DictSettingsViewModel DictSettingsView {
			get => _dictSettingsViewModel;
			set => this.RaiseAndSetIfChanged(ref _dictSettingsViewModel, value);
		}
		public int SideBarSelectedIndex {
			get => _sideBarSelectedIndex;
			set {
				_previousCurrentView = _sideBarSelectedIndex;
				_sideBarSelectedIndex = value;
				
				ChangeContent();
			}
		}
		private int _sideBarSelectedIndex;
		private core.Core _core;
		private ViewModelBase _content;
		private TranslateViewModel _translateViewModel;
		private DictSettingsViewModel _dictSettingsViewModel;
		private bool _isDictSettingsViewVisible;
		private bool _isTranslateViewVisible;
		private int _previousCurrentView;

		public MainWindowViewModel() {
			_core = new core.Core();
			_translateViewModel = new TranslateViewModel(_core);
			_dictSettingsViewModel = new DictSettingsViewModel(_core);
			_content = _translateViewModel;
			_isDictSettingsViewVisible = false;
			_isTranslateViewVisible = false;
			_sideBarSelectedIndex = 0;

			SideBarItems = new List<string> () {
				"Translate",
				"Dict settings"
			};
		}


		private void ChangeContent() {
			if(_previousCurrentView != _sideBarSelectedIndex) {
				switch(_sideBarSelectedIndex) {
					case 0:
						Content = TranslateView;
						break;
					case 1:
						Content = DictSettingsView;
						break;
				}
			}
		}

		private void DisableAllViews() {
			IsDictSettignsViewVisible = false;
			IsTransalteViewVisible = false;
		}
	}
}

// vim: sw=2 ts=2 noet
