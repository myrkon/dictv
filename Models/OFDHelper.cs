using System.Collections.Generic;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;

namespace DictV.Models {
	class OFDHelper {
		private List<FileDialogFilter> _fdlList;
		public List<string> Result { get; set; }

		public OFDHelper() {
			_fdlList = new List<FileDialogFilter>();
			FileDialogFilter fdf = new FileDialogFilter();
			fdf.Extensions.Add("dsl.dz");
			fdf.Extensions.Add("dsl");
			fdf.Name = "DSL Dict";
			_fdlList.Add(fdf);
			fdf = new FileDialogFilter();
			fdf.Extensions.Add("*");
			fdf.Name = "All";
			_fdlList.Add(fdf);
			Result = new List<string>();
		}

		public Window GetWindow() {
			Window mainWindow;
			if (Avalonia.Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop) {
				mainWindow = desktop.MainWindow;
			} else {
				mainWindow = new Window();
			}
			return mainWindow;
		}

		public OpenFileDialog GetOFD() {
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.AllowMultiple = true;
			ofd.Filters = _fdlList;
			return ofd;
		}
	}
}
