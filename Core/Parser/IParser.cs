using System.Collections.Generic;

namespace core
{
	interface IParser
	{
		public Dictionary<string, CoreArticle> Parse();
		public CoreArticle GetArticleFromPosition(int position);
		public Dictionary<string, int> IndexDict();
		public DictMetaData GetMeta();
	}
}
