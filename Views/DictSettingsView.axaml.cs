using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace DictV.Views
{
	public partial class DictSettingsView : UserControl
	{
		public DictSettingsView()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}