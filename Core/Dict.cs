using System;
using System.Collections.Generic;
using System.IO;

namespace core
{
	class Dict
	{
		public DictMetaData Metadata { get; set; }

		private FileInfo _dictFile;
		private IParser _parser;
		private string _dictIndexPath;
		private Dictionary<string, int> _deserializedArticles;

		public Dict(string dictFilePath) {
			_dictFile = new FileInfo(dictFilePath);
			Metadata = new DictMetaData();
			_parser = DetermineParserType();
			Metadata = _parser.GetMeta();
			_dictIndexPath = $"{Settings.IndexingDirectory}{Metadata.Name}.json";
			_deserializedArticles = new Dictionary<string, int>();

			if (File.Exists(_dictIndexPath)) {
				ReadIndexedDict();
			}
		}

		public Dict(FileInfo dictFile)
		{
			_dictFile = dictFile;
			_parser = DetermineParserType();
			Metadata = _parser.GetMeta();
			_dictIndexPath = $"{Settings.IndexingDirectory}{Metadata.Name}.json";
			_deserializedArticles = new Dictionary<string, int>();

			if (File.Exists(_dictIndexPath)) {
				ReadIndexedDict();
			}
		}

		private void Setup() {
		}

		public void IndexDict() {
			Utils.WriteJson(_parser?.IndexDict(), _dictIndexPath);
		}

		public void ReadIndexedDict() {
			_deserializedArticles = Utils.ReadJson<Dictionary<string, int>>(_dictIndexPath) 
				?? new Dictionary<string, int>();
			if (_deserializedArticles.Count == 0) {
				Console.WriteLine("reading the file failed");
			}
		}

		public bool ArticleExist(string name) {
			if(_deserializedArticles.ContainsKey(name)) {
				return true;
			} else {
				return false;
			}
		}

		public CoreArticle GetArticle(int indexedArticle) {
			return _parser.GetArticleFromPosition(indexedArticle);
		}

		public CoreArticle GetArticle(string word) {
			return _parser.GetArticleFromPosition(_deserializedArticles[word]);
		}

		private IParser DetermineParserType()
		{
			return new DslParser(_dictFile, Utils.GetEncoding(_dictFile)); 
		}
	}
}
