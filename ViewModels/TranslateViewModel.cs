using System.Collections.ObjectModel;
using System.Threading.Tasks;
using DictV.Models;
using ReactiveUI;
using core;

namespace DictV.ViewModels {
	public class TranslateViewModel : ViewModelBase {
		public IReactiveCommand SearchWordCommand { get; }
		public string SearchTextBox {
			get => _searchTextBox;
			set => this.RaiseAndSetIfChanged(ref _searchTextBox, value);
		}
		public ObservableCollection<ArticleItem> Items {
			get => _items;
			set => this.RaiseAndSetIfChanged(ref _items, value);
		}
		private ObservableCollection<ArticleItem> _items;
		private string _searchTextBox;
		private Core _core;

		public TranslateViewModel(Core core) {
			_core = core;
			_searchTextBox = "";
			_items = new ObservableCollection<ArticleItem>();

			SearchWordCommand = ReactiveCommand.Create(() => SearchWord());
		}

		public void SearchWord() {
			Items.Clear();
			Task.Run(() => {
				foreach (CoreArticle coreArticle in _core.GetArticle(SearchTextBox)) {
					Items.Add(new ArticleItem(coreArticle));
				}
			});
		}
	}
}
// vim: sw=2 ts=2 noet
