using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace core {
	enum ArticleStates {
		Example,
		Translation,
		TranslationMultiline,
		Init
	}

	enum States {
		Meta,
		Header,
		Body
	}

	public class DictMetaData {
		public bool Enabled { get; set; }
		public string Path { get; set; }
		public string Name { get; set; }
		public string SourceLanguage { get; set; }
		public string TargretLanguage { get; set; }

		public DictMetaData() {
			Path = "";
			Name = "";
			SourceLanguage = "";
			TargretLanguage = "";
		}
	}
	
	public class Explanation {
		public string Translations { get; set; }
		public string Examples { get; set; }

		public Explanation() {
			Translations = "";
			Examples = "";
		}
	}
	
	public class CoreArticle {
		public List<string> Titles { get; set; }
		public string? Transcription { get; set; }
		public List<string> Examples { get; set; }
		public List<string> Translations { get; set; }
		public int Position { get; set; }
		public string DictName { get; set; }

		public CoreArticle() {
			Titles = new List<string>();
			Examples = new List<string>();
			Translations = new List<string>();
			DictName = "";
		}
	}

	class Regexes {
		public static Regex DictMetaRegex = new Regex(@"^\#[^\n]*");
		public static Regex TranscriptionRegex = new Regex(@"\[t\](.*)\[\/t\]");
		public static Regex ExampleRegex = new Regex(@"\[ex\](.*)\[\/ex\]");
		public static Regex TranslationRegex = new Regex(@"\[trn\](.*)\[\/trn\]");
		public static Regex TranslationSeparateRegex = new Regex(@"(\[trn\]|\[\/trn\])");
		public static Regex LangByIdRegex = new Regex(@"\[lang id=(\d{1,5})\](.*)\[\/lang\](.*)");
		public static Regex LangTargetExampleCleanerRegex = new Regex(@"^\s?[-|—]\s?");
		public static Regex FormatCleanerRegex = new Regex(@"\[(p|\/p|i|/i|com|\/com|'|\/'|\'|\/\')\]");
		public static Regex RemoveQuadBracketsRegex = new Regex(@"\[[^\]]*]");
		public static Regex RemoveTriangleBracketsRegex = new Regex(@"\<[^\>]*>");
		public static Regex StartOfLineFromCharRegex = new Regex(@"^[^\s#]"); 
		public static Regex CommentRegex = new Regex(@"\[c\](.*)\[\/c\]");
	}

	class Settings {
		public static string ProgramDataDirectory {
			get {
				if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
					return Environment.GetEnvironmentVariable("HOME") + "/.cache/DictV/";
				} else {
					return Path.GetTempPath() + "DictV/";
				}
			}
		}
		public readonly static string IndexingDirectory = ProgramDataDirectory + "index/";
		public readonly static string SettingsFile = ProgramDataDirectory + "settings.json";
	}

	class SerializableSettings {
		public List<DictMetaData> DictMeta { get; set;}

		public SerializableSettings() {
			DictMeta = new List<DictMetaData>();
		}
	}

}
