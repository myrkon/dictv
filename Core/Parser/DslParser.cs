using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace core {
	class DslParser : IParser {
		private FileInfo _dictFile;
		private Encoding _dictionaryEncoding;
		private States _state;
		private ArticleStates _articleState;
		private CoreArticle _currentArticle;
		private List<string> _translations;
		private List<string> _examples;
		private List<Explanation> _explanations;
		private DictMetaData _metas;
		private Dictionary<string, CoreArticle> _allArticles;
		private int _lineCounter = 0;

		private string _prevLine;


		public DslParser(FileInfo dictFile, Encoding sourceEncoding) {
			_dictFile = dictFile;
			_dictionaryEncoding = sourceEncoding;
			_state = States.Meta;
			_articleState = ArticleStates.Init;
			_currentArticle = new CoreArticle();
			_translations = new List<string>();
			_examples = new List<string>();
			_explanations = new List<Explanation>();
			_metas = new DictMetaData();
			_allArticles = new Dictionary<string, CoreArticle>();

			_metas.Path = dictFile.ToString();
			_metas.Enabled = true;
			_prevLine = "";
		}

		CoreArticle IParser.GetArticleFromPosition(int position) {
			CoreArticle article = new CoreArticle();
			int lineCounter = 0;
			using (StreamReader sr = Utils.GetStreamReader(_dictFile)) {
				string? line;
				while ((line = sr.ReadLine()) != null) {
					if (lineCounter >= position) {
						if (Regexes.StartOfLineFromCharRegex.IsMatch(line)) {
							if (_state == States.Header || _state == States.Meta) {
								_currentArticle.Titles.Add(line.Trim());
							} else if (_state == States.Body) {
								_currentArticle.DictName = _metas.Name;
								AddTranslationAndExamplesToArticle();
								_translations = new List<string>();
								_examples = new List<string>();
								_explanations = new List<Explanation>();
								break;
							}
						}
						else {
							ParseBody(line);
						}
					}
					lineCounter++;
				}
				sr.Close();
			}
			_state = States.Meta;
			_articleState = ArticleStates.Init;
			article = _currentArticle;
			_currentArticle = new CoreArticle();
			return article;
		}

		DictMetaData IParser.GetMeta() {
			using (StreamReader sr = Utils.GetStreamReader(_dictFile)) {
				string? line;

				while ((line = sr.ReadLine()) != null) {
					if (Regexes.DictMetaRegex.IsMatch(line)) {
						ParseMeta(line);
					} else {
						break;
					}
				}
			}
			return _metas;
		}

		Dictionary<string, int> IParser.IndexDict() {
			Dictionary<string, int> index = new Dictionary<string, int>();
			_lineCounter = 0;
			List<string> titles = new List<string>();
			int currentArticlePosition = -1;

			using (StreamReader file = Utils.GetStreamReader(_dictFile)) {
				string? line;
				while ((line = file.ReadLine()) != null) {
					bool startOfLineFromChar = Regexes.StartOfLineFromCharRegex.IsMatch(line);
					if (startOfLineFromChar && !Regexes.DictMetaRegex.IsMatch(line)) {
							if (_state == States.Meta) {
									_state = States.Header;
									titles.Add(line.Trim());
									currentArticlePosition = _lineCounter;
							}
							else if (_state == States.Header) {
								titles.Add(line.Trim());
							} else if (_state == States.Body) {
								for (int i=0; i < titles.Count; i++) {
									index.Add(titles[i], currentArticlePosition + i);
								}

								titles = new List<string>();
								currentArticlePosition = _lineCounter;
								titles.Add(line.Trim());

								_state = States.Header;
							}
					}
					if (!startOfLineFromChar && _state == States.Header) _state = States.Body;

					_lineCounter++;
				}
			}
			
			_state = States.Meta;
			_articleState = ArticleStates.Init;
			return index;
		}

		Dictionary<string, CoreArticle> IParser.Parse() {
			using (StreamReader file = Utils.GetStreamReader(_dictFile)) {
				string? line;
				
				while((line = file.ReadLine()) != null) {
						if(Regexes.DictMetaRegex.IsMatch(line)) {
								ParseMeta(line);
						} else if (Regexes.StartOfLineFromCharRegex.IsMatch(line)) {
								ParseHeader(line);
						} else if(!Regexes.StartOfLineFromCharRegex.IsMatch(line)) {
								ParseBody(line);
						}

						_prevLine = line;
						_lineCounter++;
				}
				file.Close();
			}
			_state = States.Meta;
			_articleState = ArticleStates.Init;
			return _allArticles;
		}

		string RemoveTags(string line) {
			string result = Regexes.RemoveQuadBracketsRegex.Replace(line, "");
			result = Regexes.RemoveTriangleBracketsRegex.Replace(result, "");
			return result.Replace("•", "");
		}

		Explanation PackExplanation(string translation, string example) {
			return new Explanation() {
					Translations = translation,
					Examples = example
			};
		}

		void ParseMeta(string line) {
			_state = States.Meta;
			string[] splitted = line.Split("\"");
			string metaName = splitted[0].Replace(" ", "") .Replace("#", "").Trim();
			string metaValue = splitted[1].Trim();

			if (metaName == "NAME") {
				_metas.Name = metaValue;
			} else if (metaName == "INDEX_LANGUAGE") {
				_metas.SourceLanguage = metaValue;
			} else if (metaName == "CONTENTS_LANGUAGE") {
				_metas.TargretLanguage = metaValue;
			}
		}

		void ParseHeader(string line) {
			if(_state == States.Meta) {
				_state = States.Header;
				_currentArticle.Titles.Add(line.Trim());
				_currentArticle.Position = _lineCounter;
			} 
			else if(_state == States.Header) 
				_currentArticle.Titles.Add(line.Trim());
			else if(_state == States.Body) {
				_currentArticle.DictName = _metas.Name;
				AddTranslationAndExamplesToArticle();

				foreach(string title in _currentArticle.Titles)
						_allArticles.Add(title, _currentArticle);

				_currentArticle = new CoreArticle();
				_translations = new List<string>();
				_examples = new List<string>();
				_explanations = new List<Explanation>();

				_currentArticle.Titles.Add(line.Trim());
				_currentArticle.Position = _lineCounter;

				_state = States.Header;
			}
		}
				
		private void ParseBody(string line) {
			_state = States.Body;
			line = line.Trim();
			bool translationsRegexMatched = Regexes.TranslationRegex.IsMatch(line);

			if(Regexes.TranscriptionRegex.IsMatch(line)) 
				_currentArticle.Transcription = Regexes.TranscriptionRegex.Match(line).Groups[1].Value;

			if(translationsRegexMatched) {
				if(_articleState == ArticleStates.Example) {
					AddTranslationAndExamplesToArticle();
					_translations.Clear();
					_examples.Clear();
				}

				_articleState = ArticleStates.Translation;
				if (line.Trim() != "") {
					_translations.Add(RemoveTags(line));
				}
			}

			if(!translationsRegexMatched && Regexes.TranslationSeparateRegex.IsMatch(line)
				 || _articleState == ArticleStates.TranslationMultiline) {
				_articleState = ArticleStates.TranslationMultiline;
				if (line.Trim() != "") {
					_translations.Add(RemoveTags(line));
				}
			}

			if(Regexes.ExampleRegex.IsMatch(line)) {
				_articleState = ArticleStates.Example;
				if (line.Trim() != "") {
					_examples.Add(RemoveTags(line));
				}
			}

			if(Regexes.CommentRegex.IsMatch(line)) {
				_articleState = ArticleStates.Translation;
				MatchCollection matches = Regexes.CommentRegex.Matches(line);
				foreach(Match match in matches) 
						_translations.Add(match.Groups[1].Value);
			}

			if(_articleState == ArticleStates.Init) _translations.Add(RemoveTags(line));
		}

		private void AddTranslationAndExamplesToArticle() {
			_currentArticle.Translations.AddRange(_translations);
			_currentArticle.Examples.AddRange(_examples);
		}
	}
}