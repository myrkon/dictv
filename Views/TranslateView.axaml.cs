using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using DictV.ViewModels;

namespace DictV.Views
{
	public partial class TranslateView : UserControl
	{
		public TranslateView()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}

		private void TextBoxKeyDown(object sender, KeyEventArgs keyEventArgs) {
			if (keyEventArgs.Key == Key.Enter) {
				var vm = (TranslateViewModel?)this.DataContext ;
				vm?.SearchWord();
			}
		}
	}
}