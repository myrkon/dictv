using System.Collections.Generic;

namespace DictV.Models {
	public class ArticleItem {
		public string Title { get; set; }
		public List<string> Translations { get; }
		public List<string> Examples { get; }
		public string DictName { get; set; }

		public ArticleItem(core.CoreArticle coreArticle) {
			Title = coreArticle.Titles[0];
			Translations = coreArticle.Translations;
			Examples = coreArticle.Examples;
			DictName = coreArticle.DictName;
		}
	}
}
