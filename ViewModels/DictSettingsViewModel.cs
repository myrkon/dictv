using System.Collections.ObjectModel;
using ReactiveUI;
using core;
using DictV.Models;
using Avalonia.Controls.Selection;
using System.Collections.Generic;
using System;

namespace DictV.ViewModels {
	public class DictSettingsViewModel : ViewModelBase {
		public SelectionModel<string> Selection { get; set;}
		public ObservableCollection<string> DictList {
			get => _dictList;
			set => this.RaiseAndSetIfChanged(ref _dictList, value);
		}
		private ObservableCollection<string> _dictList;
		private Core _core;

		public DictSettingsViewModel(Core core) {
			_dictList = new ObservableCollection<string>();
			_core = core;
			Selection = new SelectionModel<string>();
			Selection.SingleSelect = false;

			LoadDictList();
		}

		public void LoadDictList() {
			foreach (DictMetaData dict in _core.GetDicts()) {
				_dictList.Add(dict.Name);
			}
		}

		public async void AddDicts() {
			OFDHelper ofdHelper = new OFDHelper();
			string[] result = await ofdHelper.GetOFD().ShowAsync(ofdHelper.GetWindow());

			if (result != null) {
				foreach (string path in result) {
					DictMetaData dictMetaData = _core.AddDict(path);
					_dictList.Add(dictMetaData.Name);
				}
				_core.WriteSettings();
			}
		}

		public void RemoveDict() {
			if (Selection.SelectedIndexes.Count > 0) {
				List<string> selectedItems = new List<string>(Selection.SelectedItems);
				foreach (string item in selectedItems) {
					_core.RemoveDict(item);
					_dictList.Remove(item);
					_core.WriteSettings();
				}
			}
		}

		public void IndexDicts() {
			_core.IndexDicts();
			_core.WriteSettings();
		}

		public void RaiseDict() {
			if (Selection.SelectedIndexes.Count > 0) {
				List<string> selectedItems = new List<string>(Selection.SelectedItems);
				foreach (string item in selectedItems) {
					MoveDictInList(item, -1);
				}
				ShiftSelection(-1);
			}
		}

		public void LowerDict() {
			if (Selection.SelectedIndexes.Count > 0) {
				List<string> selectedItems = new List<string>(Selection.SelectedItems);
				selectedItems.Reverse();
				foreach (string item in selectedItems) {
					MoveDictInList(item, 1);
				}

				ShiftSelection(1);
			}
		}
		private void ShiftSelection(int direction) {
				List<int> selectedIndexes = new List<int>(Selection.SelectedIndexes);
				Selection.Clear();
				foreach (int index in selectedIndexes) {
					Selection.Select(index+direction);
				}
		}

		private void MoveDictInList(string item, int direction) {
			int index = 0;
			var dictList = new List<string>(_dictList);
			foreach (var dictItem in dictList) {
				if(dictItem == item) {
					if(VerifyIndex(index, direction)) {
						_dictList.Move(index, index+direction);
						_core.MoveDict(index, index+direction);
					}
					break;
				}
				index++;
			}
		}

		private bool VerifyIndex(int index, int direction) {
			if (direction > 0 && index < _dictList.Count-1) {
				return true;
			} else if (direction < 0 && index > 0) {
				return true;
			} else {
				return false;
			}
		}
	}
}
// vim: sw=2 ts=2 noet
