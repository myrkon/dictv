dictv
-----

I planning write cross-platform Golden Dict analog with support of:

1. UTF-8 DSL
2. UTF-16 DSL
3. Compressed DSL (.dsl.dz)
4. aaand may be LSD..

Also, I will write small gui utility for work with dicts:

- Converting from first encoding, to second
- Compress\Decompress
- Merge many dicts into one
- **May be** converting between formats.

### To-do list
- [X] Write DSL Parser
- [X] Write support of compressed dicts.
- [X] Write GUI
- [ ] Write GUI for dict utility.

#### Optional
- [ ] LSD Parser
- [ ] Converting between formats


### Compiling
I do not know for what, but I'm leave it here.
``dotnet build``
``dotnet run``
