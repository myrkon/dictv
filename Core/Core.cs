using System;
using System.Collections.Generic;
using System.IO;

namespace core {
	public class Core {
		private SerializableSettings _settings;
		private List<Dict> _dicts;

		public Core() {
			_settings =	new SerializableSettings();
			_dicts = new List<Dict>();

			if (!Directory.Exists(Settings.ProgramDataDirectory)) {
				Directory.CreateDirectory(Settings.ProgramDataDirectory);
			} 
			if (!Directory.Exists(Settings.IndexingDirectory)) {
				Directory.CreateDirectory(Settings.IndexingDirectory);
			}

			if(File.Exists(Settings.SettingsFile)) {
				LoadSettings();
			}
		}

		public bool LoadSettings() {
			var nullableVar = Utils.ReadJson<SerializableSettings>(Settings.SettingsFile);

			if (nullableVar is SerializableSettings) {
				_settings = nullableVar;
			} else {
				Console.WriteLine("attempt of reading settings file failed");
				return false;
			}

			foreach (DictMetaData dictMetaData in _settings.DictMeta) {
				Console.WriteLine($"{dictMetaData.Path} loaded..");
				_dicts.Add(new Dict(dictMetaData.Path));
			}

			return true;
		}

		public void WriteSettings() {
			Utils.WriteJson(_settings, Settings.SettingsFile);
		}

		public DictMetaData AddDict(string dictFilePath) {
			Dict dict = new Dict(dictFilePath);
			_settings.DictMeta.Add(dict.Metadata);
			_dicts.Add(dict);
			return dict.Metadata;
		}

		public void RemoveDict(int index) {
			_dicts.RemoveAt(index);
			_settings.DictMeta.RemoveAt(index);
		}

		public void RemoveDict(string item) {
			foreach (Dict dict in _dicts) {
				if (dict.Metadata.Name == item) {
					_dicts.Remove(dict);
					break;
				}
			}

			foreach (DictMetaData dictMetaData in _settings.DictMeta) {
				if (dictMetaData.Name == item) {
					_settings.DictMeta.Remove(dictMetaData);
					break;
				}
			}
		}

		public void MoveDict(int oldIndex, int newIndex) {
			Dict item = _dicts[oldIndex];
			_dicts.RemoveAt(oldIndex);
			_dicts.Insert(newIndex, item);
		}

		public IEnumerable<CoreArticle> GetArticle(string word) {
			for (int i=0; i < _dicts.Count; i++) {
				if (_settings.DictMeta[i].Enabled && _dicts[i].ArticleExist(word)) {
					yield return _dicts[i].GetArticle(word);
				}
			}
		}

		public void SetDictionaryState(bool state, int idx) {
			_settings.DictMeta[idx].Enabled = state;
		}

		public List<DictMetaData> GetDicts() {
			return _settings.DictMeta;
		}

		public void IndexDicts() {
			foreach (Dict dict in _dicts) {
				dict.IndexDict();
			}
		}
	}
}
